#!/bin/bash
#SBATCH -c 8
#SBATCH -N 1
#SBATCH -J bs-danio-code
#SBATCH -o slurm-%J.dname-danio-code.out
#SBATCH -e slurm-%J.dname-danio-code.err
#SBATCH --mem 10G

# usage: read1, read2, index.tgz, chrom.size

echoerr() { cat <<< "$@" 1>&2; }

read1=$1
read2=$2
index=$3
index_name=$(basename $index)
chrom_size=$4

read1_root=$(basename $read1)
read1_root=${read1_root%.gz}
read1_root=${read1_root%.fastq}
read1_root=${read1_root%.fq}
read2_root=$(basename $read2)
read2_root=${read2_root%.gz}
read2_root=${read2_root%.fastq}
read2_root=${read2_root%.fq}
bam_root=${read1_root}_${read2_root}

# copy index.tgz, because dname_align_pe will delete it. If ythe index is in the current dir
# copy it to the temp archive, which will be reverted to $index

echo "Pipeline started at $(date)"

if [ ! -f ./input.tgz ]
then
    cp $index ./input.tgz
    cp ./input.tgz ./temp.tgz
else
    cp $index ./temp.tgz
fi

echoerr $pwd

echo "Starting the alignment at $(date)"
../wgbs/dme-align-pe/dname_align_pe.sh ./input.tgz $read1 $read2 100 5000 $SLURM_CPUS_PER_TASK $bam_root

echo "Finished the alignment at $(date)"
echo $(sstat --format

if [ -e ./temp.tgz ]
then
    cp temp.tgz ./input.tgz
fi

echo "Starting methylation extraction at $(date)"
../wgbs/dme-extract-pe/dname_extract_pe.sh ./input.tgz ${bam_root}_bismark_pe.bam $SLURM_CPUS_PER_TASK

echo "Finished methylation extraction at $(date)"
cx_fl=${bam_root}_bismark_pe.CX_report.txt.gz
echo $cx_fl
target_root=${cx_fl%.CX_report.txt.gz}
echo $target_root
gunzip ${target_root}.CX_report.txt.gz

echo "==>Start CX report to bed<=="
ls -lh | grep ${target_root}.CX_report.txt -
../wgbs/dme-cx-to-bed/dname_cx_to_bed.sh ${target_root}.CX_report.txt $chrom_size

cx_fl=${bam_root}_bismark_pe.bedGraph.gz
echo $cx_fl
target_root=${cx_fl%.bedGraph.gz}
echo $target_root
gunzip ${target_root}.bedGraph.gz 

../wgbs/dme-bg-to-signal/dname_bg_to_signal.sh ${target_root}.bedGraph $chrom_size
