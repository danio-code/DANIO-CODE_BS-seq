workflow DANIOCODE_WGBS {

	File reads
	File index
	File chrSizes
	String bamRoot
	#String rrbs

	call bismarkAlign {
		input: reads=reads, index=index, bamRoot=bamRoot
	}
	call extractMethylation {
		input: bam=bismarkAlign.bamOut, mapReport=bismarkAlign.report, index=index, bamRoot=bamRoot 
	}
	call generateSignal {
		input: bg=extractMethylation.bedGraph, chrSizes=chrSizes, bamRoot=bamRoot
	}
	call generateRegion {
		input: cxReport=extractMethylation.cxReport, chrSizes=chrSizes, bamRoot=bamRoot

	}

}

task bismarkAlign{

	File reads
	File index
	String bamRoot

	command {
		../wgbs/dme-align-pe/dname_align_pe.sh ${index} ${reads} 8 ${bamRoot}
	}

	output {
		File bamOut="${bamRoot}.bam"
		File report="${bamRoot}_map_report.txt"
		File qc="${bamRoot}_qc.txt"
	}
}

task extractMethylation {

	File bam
	File mapReport
	File index
	String bamRoot

	command {
		../wgbs/dme-extract-pe/dname_extract_se.sh ${index} ${bam} 8
	}

	output {
		File cxReport="${bamRoot}.CX_report.txt.gz"
		File bedGraph="${bamRoot}.bedGraph.gz"
		File mBias="${bamRoot}_mbias_report.txt"

	}
}

task generateRegion {

	File cxReport
	File chrSizes
	String bamRoot

	command {
		../wgbs/dme-cx-to-bed/dname_cx_to_bed.sh ${cxReport} ${chrSizes}
	}

	output {
		File cpgBed="${bamRoot}_CpG.bed"
		File cpgBigBed="${bamRoot}_CpG.bb"
		File chgBed="${bamRoot}_CHG.bed"
		File chgBigBed="${bamRoot}_CHG.bb"
		File chhBed="${bamRoot}_CHH.bed"
		File chhBigBed="${bamRoot}_CHH.bb"
	}
}

task generateSignal {

	File bg
	File chrSizes
	String bamRoot

	command {
		../wgbs/dme-bg-to-signal/dname_bg_to_signal.sh ${bg}.bedGraph ${chrSizes}
	}

	output {
		File bigWig="${bamRoot}.bigWig"
	}
}