# DANIO-CODE BS-seq pipeline v1.0

Bisulfite treatment coupled with whole genome sequencing (WGBS-seq) is a 
powerful assay to detect DNA methylation at a single nucleotide resolution 
genomewide. The method is based on treatment of genomic DNA with sodium 
bisulfite, which converts unmethylated cytosine to tyrosine after a subsequent 
PCR amplification (Chattarjee et al., 2012).

## Description of the pipeline

The WGBS-seq processing pipeline used is the ENCODE pipeline adapted for the 
analysis of _Danio rerio_ bisulfite data. The pipeline consists of several 
steps, including quality trimming and filtering of raw reads, aligning reads to
the C → T and G → A transformed and indexed genome respectively using Bismarck. 
The raw methylation data are showed in bigWig file obtained with 
`coverage2cytosine` tool, and extracted methylation are obtained with 
`bismark_methylation_extractor` tool which produce methylation report in
CpG, CHG and CHH context. Quality metrics for each stage of the pipeline are 
included in the outputs. More about the pipeline can be found on this link:
https://www.encodeproject.org/wgbs/ .

## Pipeline workflow

The pipeline consists of several independent parts. The pipeline can be run
on DNAnexus environment, as well as DNAnexus independent. 

The workflow of the pipeline is as following:

1. Align reads with Bismark/Bowtie 2:
   The part of the program which quality trims and filter reads, and mapps the
   mapps the reads to the reference genome.
   1. Inputs:
      1. Raw sequncing reads (in fastq format)
      2. Indexed converted (C->T, G->A) genomes in an archive file
   2. Outputs:
      1. mapped bismark output (*.bam)
      2. Text file with `samtools flagstats` QC metrics (*_qc.txt)
      3. Mapping QC report (*_map_report.txt)
2. Extract methylation:
   Extract methylation and report WGBS Analysis.
   1. Inputs:
      1. Mapped bismark output (1.2.1)
      2. Mapping QC report (1.2.3)
      3. Indexed converted (C->T, G->A) genomes in an archive file
   2. Outputs:
      1. Text file with `samtools flagstats` QC metrics (*_qc.txt)
      2. Mapping QC report (*_map_report.txt)
      3. M-bias QC report (*_mbias_report.txt)
      4. Context report from bismark extraction (*.CX_report.txt.gz)
      5. BedGraph from bismark extraction (*.bedGraph.gz)
3. Convert methylation bedGraph to bigWig:
  Create a bigWig signal file from a BedGraph file
   1. Inputs:
      1. BedGraph from bismark extraction (2.2.5)
      2. Chromosome name/length file
   2. Outputs:
      1. Signal file in bigWig format (*.bw)
4. Convert methylation context to bed files:
   Converts the comtext file from bismark extraction for each context 
   (CpG, CHG and CHH) separately, and creates files in bed format and bigBed
   1. Inputs:
      1. Context report from bismark extraction (2.2.4)
      2. Chromosome name/length file
   2. Outputs:
      1. CpG methylation BED file (*_CpG.bed.gz)
      2. CHG methylation BED file (*_CHG.bed.gz)
      3. CHH methylation BED file (*_CHH.bed.gz)
      4. CpG methylation bigBed file (*_CpG.bb)
      5. CHG methylation bigBed file (*_CHG.bb)
      6. CHH methylation bigBed file (*_CHH.bb)

## Instructions to run:

### 1. DNAnexus platform

To run the pipeline on the DNAnexus platform it is neccessary to construct build 
the Apps on the platform. That could be done by runing the script `build_applets`
in the `dna-me-pipeline/dnanexus/` folder (dx-toolkit required).

The pipeline can be run by constructing a workflow which follows the described scheme.
Here is the list of Apps which perform each step:

1. Align reads with Bismark/Bowtie 2:
   * `dme-align-pe` for paired-end data
   * `dme-align-se` for single-end data
2. Extract methylation:
   * `dme-extract-se`
3. Convert methylation bedGraph to bigWig
   * `dme-bg-to-signal`
4. Convert methylation context to bed files
   * `dme-cx-to-bed`

The genome index file can be constructed by running the `dme-index-bismark-bowtie2`
App. It takes gzipped fasta genome file, gzipped lambda genome file, and a text file
with list of chromosome sizes as th imput, and outputs the indexed converted 
(C->T, G->A) genomes in an archive file.

### 2. DNAnexus independent

It is possible to run the Apps outside the DNAnexus platform. Each App consists
of the DNAnexus independent script, which handles the computation. Those 
scripts can be found in the `wgbs/` folder. Except for the script, each folder
contains the executables of the dependencies as well.
An example script to run the pipeline on the slurm scheduler is present in the `slurm/`
folder. Please, note the issues present for the run, in the Issues tab.

