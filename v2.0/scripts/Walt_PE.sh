#uses the output files from Trim_PE 
#change WALT_DIR, DBindex_DIR  AND SAMBAMBA_DIR.

WALT_DIR="/PATH/"
DBindex_DIR="/PATH/"
SAMBAMBA_DIR="/PATH/"
   
 for read_1 in *_1_trimmed_paired.fastq.gz; do

	echo $read_1
        read_2=$(echo $read_1 | sed s/_1_paired.fastq.gz/_2_paired.fastq.gz/)
        echo $read_2
		read_3=$(echo $read_1 | rev | cut -c 4- | rev)
		read_4=$(echo $read_2 | rev | cut -c 4- | rev)
        ext='PE.sam'  # change ext to reflect genome assembly
		ext2='PE_sorted.bam' # change ext to reflect genome assembly
        output_sam=$(echo $read_1 | rev | cut -c 18- | rev)$ext 
		output_bam=$(echo $read_1 | rev | cut -c 18- | rev)$ext2
gunzip -c $read_1 > $read_3  #gunzip fastq files but keep a zipped copy, as the unzipped will be deleted soon
gunzip -c $read_2 > $read_4

 ${WALT_DIR}walt -i {DBindex_DIR}genome.dbindex  -1 $read_3 -2 $read_4 -o $output_sam  -t 24 -N 10000000 -L 2000
 
	rm $read_3
	rm $read_4
	
	#convert sam to bam and sort
	
	${SAMBAMBA_DIR}sambamba view -t 16 -S -f bam -h $output_sam| ${SAMBAMBA_DIR}sambamba sort -t 16 -o $output_bam /dev/stdin

	rm $output_sam  #optional step to save space

	done 
